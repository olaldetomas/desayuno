import Vue from 'vue'
import App from './App.vue'
import router from './router/index'

// Font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faSortDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import '@babel/polyfill'
import vuetify from './plugins/vuetify';


Vue.component('font-awesome-icon', FontAwesomeIcon);
library.add(faUser);
library.add(faSortDown);
library.add(faBars);

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
